Objective:
Today, I learned about Integration testing and API testing of the service layer using Spring Boot, and practiced several test cases.

Reflective:
During the learning process, I found that I did not know much about the unit test of writing Spring Boot, especially in the use of Mock object for testing. This made me realize that in agile development, writing effective tests is crucial as it can help us quickly detect and fix problems.

Interpretive:
Through today's learning, I realized the importance of testing in software development. Integration testing and API testing can help us verify the function and interaction of the whole system, while unit test can verify independent code modules. Mastering these testing techniques can improve development efficiency and code quality.

Decisional:
In order to improve my abilities in agile development, I have decided to strengthen my learning and practice of testing in Spring Boot. I will do more practice, especially in using Mock for unit testing. I will also search for relevant materials and resources to help me better master testing techniques and apply them to practical projects. Through continuous practice and learning, I believe I can improve my agile development skills.