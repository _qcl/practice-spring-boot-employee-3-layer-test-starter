package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {

    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        employeeRepository.clearAll();
        companyRepository.clearAll();
    }

    @Test
    void should_get_all_company_when_perform_get_given_companies() throws Exception {
        //given
        Employee susan = buildEmployeeSusan();
        Company company = new Company(1,"myCompany"
                ,new ArrayList<>(List.of(susan)));
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("myCompany"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(susan.getId()))
                .andExpect(jsonPath("$[0].employees[0].name").value(susan.getName()))
                .andExpect(jsonPath("$[0].employees[0].age").value(susan.getAge()))
                .andExpect(jsonPath("$[0].employees[0].gender").value(susan.getGender()))
                .andExpect(jsonPath("$[0].employees[0].salary").value(susan.getSalary()));
    }

    @Test
    void should_get_company_by_id_when_perform_get_given_companies() throws Exception {
        //given
        Employee susan = buildEmployeeSusan();
        Company company = new Company(1,"myCompany"
                ,new ArrayList<>(List.of(susan)));
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}",company.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("myCompany"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(susan.getId()))
                .andExpect(jsonPath("$.employees[0].name").value(susan.getName()))
                .andExpect(jsonPath("$.employees[0].age").value(susan.getAge()))
                .andExpect(jsonPath("$.employees[0].gender").value(susan.getGender()))
                .andExpect(jsonPath("$.employees[0].salary").value(susan.getSalary()));
    }

    @Test
    void should_get_employees_by_company_id_when_perform_get_given_companies() throws Exception {
        //given
        Employee susan = buildEmployeeSusan();
        Company company = new Company(1,"myCompany"
                ,new ArrayList<>(List.of(susan)));
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{companyId}/employees",company.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000));
    }

    @Test
    void should_get_company_by_page_when_perform_get_given_companies_and_page_and_size() throws Exception {
        //given
        Employee susan = buildEmployeeSusan();
        Employee lisi = buildEmployeeLisi();
        Company company = new Company(1,"myCompany"
                ,new ArrayList<>(List.of(susan)));
        companyRepository.addCompany(company);
        companyRepository.addCompany(new Company(2,"xxx",new ArrayList<>(List.of(lisi))));

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageIndex=2&pageSize=1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(2))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("xxx"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(lisi.getId()))
                .andExpect(jsonPath("$[0].employees[0].name").value(lisi.getName()))
                .andExpect(jsonPath("$[0].employees[0].age").value(lisi.getAge()))
                .andExpect(jsonPath("$[0].employees[0].gender").value(lisi.getGender()))
                .andExpect(jsonPath("$[0].employees[0].salary").value(lisi.getSalary()));
    }

    @Test
    void should_add_company_when_perform_post_given_companies() throws Exception {
        //given
        Employee susan = buildEmployeeSusan();
        Company company = new Company(1,"myCompany"
                ,new ArrayList<>(List.of(susan)));
        String Json = mapper.writeValueAsString(company);
        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Json))
                .andExpect(status().isOk());
    }

    @Test
    void should_remove_company_when_perform_delete_given_companies() throws Exception {
        //given
        Employee susan = buildEmployeeSusan();
        Company company = new Company(1,"myCompany"
                ,new ArrayList<>(List.of(susan)));
        companyRepository.addCompany(company);

        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}",1));

        assertTrue(companyRepository.getCompanies().isEmpty());
    }

    private static Employee buildEmployeeSusan() {
        Employee newEmployee = new Employee(1, "Susan", 22, "Female", 10000);
        return newEmployee;
    }
    private static Employee buildEmployeeLisi() {
        Employee employee2 = new Employee(2, "Lisi", 30, "Man", 5000);
        return employee2;
    }
    private static Employee buildEmployeeWangwu() {
        Employee employee3 = new Employee(3, "WangWu", 30, "Man", 6000);
        return employee3;
    }

}
