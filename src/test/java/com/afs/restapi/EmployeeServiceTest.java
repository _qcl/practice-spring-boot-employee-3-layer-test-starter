package com.afs.restapi;

import com.afs.restapi.advice.EmployeeAgeErrorException;
import com.afs.restapi.advice.EmployeeAgeNoMatchSalaryException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.advice.EmployeeNotFoundException;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {

    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_throw_age_error_exception_when_add_employees_given_employee_age_17_and_age_66() {
        Employee employeeYoung = new Employee(1, "garrick", 17, "male", 1000);
        Employee employeeOld = new Employee(2, "oldGarrick", 66, "male", 10000);

        Assertions.assertThrows(EmployeeAgeErrorException.class,() -> employeeService.addEmployee(employeeOld));
        Assertions.assertThrows(EmployeeAgeErrorException.class,() -> employeeService.addEmployee(employeeYoung));
        Mockito.verify(employeeRepository,times(0)).insert(any());
    }

    @Test
    void should_throw_age_no_match_salary_exception_when_add_employee_given_employee_age_35_salary_15000() {
        Employee employee = new Employee(1, "garrick", 35, "male", 15000);

        Assertions.assertThrows(EmployeeAgeNoMatchSalaryException.class,() -> employeeService.addEmployee(employee));
        Mockito.verify(employeeRepository,times(0)).insert(any());
    }

    @Test
    void should_set_employee_status_true_when_add_employee_given_employee() {
        Employee employee = new Employee(1, "Zhaosi", 38, "male", 35000);
        Employee employeeToReturn = new Employee(1, "Zhaosi", 38, "male", 35000);
        employeeToReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        Employee employeeSaved = employeeService.addEmployee(employee);

        Assertions.assertTrue(employeeSaved.isStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
          Assertions.assertTrue(employeeToSave.isStatus());
          return true;
        }));
    }

    @Test
    void should_set_employee_status_to_false_when_remove_employee_given_employee() {
        Employee employee = new Employee(1, "Zhaosi", 38, "male", 35000);
        employee.setStatus(true);
        when(employeeRepository.findById(employee.getId())).thenReturn(employee);

        employeeService.deleteEmployee(employee.getId());

        Assertions.assertFalse(employee.isStatus());
    }

    @Test
    void should_no_return_employees_with_status_is_false_when_get_employees_given_employee() {
        Employee employee = new Employee(1, "Gary", 23, "male", 60000);
        when(employeeRepository.findAll()).thenReturn(null);
        when(employeeRepository.findById(employee.getId())).thenReturn(null);

        Assertions.assertThrows(EmployeeNotFoundException.class, () -> employeeService.getAll());
        Assertions.assertThrows(EmployeeNotFoundException.class, () -> employeeService.getById(employee.getId()));
    }
    @Test
    void should_no_return_employees_with_status_is_false_when_modify_employees_given_employee() {
        Employee employee = new Employee(1, null, 23, null, 60000);
        when(employeeRepository.update(employee.getId(),employee)).thenReturn(null);

        Assertions.assertThrows(EmployeeNotFoundException.class,
                () -> employeeService.modifyEmployee(employee.getId(),employee));
    }
}
