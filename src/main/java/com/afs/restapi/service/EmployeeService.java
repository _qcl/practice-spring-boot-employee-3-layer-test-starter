package com.afs.restapi.service;

import com.afs.restapi.advice.EmployeeAgeErrorException;
import com.afs.restapi.advice.EmployeeAgeNoMatchSalaryException;
import com.afs.restapi.advice.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        List<Employee> employees = employeeRepository.findAll();
        if(employees == null)
            throw new EmployeeNotFoundException();
        return employees;
    }

    public Employee getById(int id) {
        Employee employee = employeeRepository.findById(id);
        if(employee == null)
            throw new EmployeeNotFoundException();
        return employee;
    }

    public List<Employee> getByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee addEmployee(Employee employee) {
        if(employee.getAge() < 18 || employee.getAge() > 65)
            throw new EmployeeAgeErrorException();
        if(employee.getAge() >= 30 && employee.getSalary() < 20000)
            throw new EmployeeAgeNoMatchSalaryException();
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee modifyEmployee(int id, Employee employee) {
        Employee updatedEmployee = employeeRepository.update(id, employee);
        if(updatedEmployee == null)
            throw new EmployeeNotFoundException();
        return updatedEmployee;
    }

    public void deleteEmployee(int id) {
        Employee employee = employeeRepository.findById(id);
        employee.setStatus(false);
    }
}